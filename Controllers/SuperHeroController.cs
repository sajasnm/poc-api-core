using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace poc_api.Controllers;



[Route("api/[controller]")]
[ApiController]
// [ApiController]
// [Route("[controller]")]
public class SuperHeroController : ControllerBase
{


    // private static List<SuperHero> heroes = new List<SuperHero>{
    //             new SuperHero {
    //                 Id=1,
    //                 Name="spiderman",
    //                 FirstName="Jhone",
    //                 LastName="Doe",
    //                 Place="Kandy"
    //              },
    //             new SuperHero {
    //                 Id=2,
    //                 Name="spiderman 2",
    //                 FirstName="2 Jhone",
    //                 LastName="2 Doe",
    //                 Place="2 Kandy"
    //              },
    //                 new SuperHero {
    //                 Id=3,
    //                 Name="spiderman 3",
    //                 FirstName="3 Jhone",
    //                 LastName="3 Doe",
    //                 Place="3 Kandy"
    //              }

    //         };

    private readonly DataContext _context;
    public SuperHeroController(DataContext dataContext)
    {
        _context = dataContext;
    }

    [HttpGet]
    public async Task<ActionResult<List<SuperHero>>> Get()
    {
        return Ok(await _context.SuperHeroes.ToListAsync());
        // return Ok(heroes);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<SuperHero>> Get(int id)
    {
        var hero = await _context.SuperHeroes.FindAsync(id);
        // var hero = heroes.Find(h => h.Id == id);
        // var hero=heroes[id];
        if (hero == null)
            return BadRequest("Hero not found.");
        return Ok(hero);
    }

    [HttpPost]
    public async Task<ActionResult<List<SuperHero>>> AddHero(SuperHero hero)
    {
        // heroes.Add(hero);
        _context.SuperHeroes.Add(hero);
        await _context.SaveChangesAsync();
        return Ok(await _context.SuperHeroes.ToListAsync());
        // return Ok(heroes);
    }

    [HttpPut]
    public async Task<ActionResult<List<SuperHero>>> UpdateHero(SuperHero request)
    {
        var dbHero = await _context.SuperHeroes.FindAsync(request.Id);
        // var hero = heroes.Find(h => h.Id == request.Id);

        // var hero=heroes[id];
        if (dbHero == null)
            return BadRequest("Hero not found.");
        // heroes.Add(hero);
        dbHero.Name = request.Name;
        dbHero.FirstName = request.FirstName;
        dbHero.LastName = request.LastName;
        dbHero.Place = request.Place;

        await _context.SaveChangesAsync();
        return Ok(await _context.SuperHeroes.ToListAsync());
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<List<SuperHero>>> DeleteHero(int id)
    {
        var dbHero = await _context.SuperHeroes.FindAsync(id);
        // var hero = heroes.Find(h => h.Id == id);
        // var hero=heroes[id];
        if (dbHero == null)
            return BadRequest("Hero not found.");
        // heroes.Remove(hero);
        _context.SuperHeroes.Remove(dbHero);
        await _context.SaveChangesAsync();
        return Ok(await _context.SuperHeroes.ToListAsync());

    }
}